<?php

use Illuminate\Http\Request;
use App\Article;
use App\Http\Middleware\Checkauth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
date_default_timezone_set("Asia/Jakarta");
header('Access-Control-Allow-Origin: *');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* articles routes */

/*Route::get('articles', function() {
    // If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    return Article::all();
});
 
Route::get('articles/{id}', function($id) {
    return Article::find($id);
});

Route::post('articles', function(Request $request) {
    return Article::create($request->all);
});

Route::put('articles/{id}', function(Request $request, $id) {
    $article = Article::findOrFail($id);
    $article->update($request->all());

    return $article;
});

Route::delete('articles/{id}', function($id) {
    Article::find($id)->delete();

    return 204;
});*/

Route::get('kemasans', 'KemasanController@index')->middleware(Checkauth::class);
Route::get('kemasan/{id}', 'KemasanController@show')->middleware(Checkauth::class);
Route::post('kemasan', 'KemasanController@store')->middleware(Checkauth::class);
Route::put('kemasan/{id}', 'KemasanController@update')->middleware(Checkauth::class);
Route::delete('kemasan/{id}', 'KemasanController@delete')->middleware(Checkauth::class);

Route::get('get_satuan', 'KemasanController@get_kemasan');
Route::get('get_obat', 'StokController@get_obat');
Route::get('get_obat_master', 'ObatController@get_obat');
Route::get('get_supplier', 'SupplierController@get_supplier');

Route::get('suppliers', 'SupplierController@index')->middleware(Checkauth::class);
Route::get('supplier/{id}', 'SupplierController@show')->middleware(Checkauth::class);
Route::post('supplier', 'SupplierController@store')->middleware(Checkauth::class);
Route::put('supplier/{id}', 'SupplierController@update')->middleware(Checkauth::class);
Route::put('supplier/update/{id}', 'SupplierController@status')->middleware(Checkauth::class);
Route::delete('supplier/{id}', 'SupplierController@delete')->middleware(Checkauth::class);

Route::get('obat', 'ObatController@index')->middleware(Checkauth::class);
Route::get('obat/{id}', 'ObatController@show')->middleware(Checkauth::class);
Route::post('obat', 'ObatController@store')->middleware(Checkauth::class);
Route::put('obat/{id}', 'ObatController@update')->middleware(Checkauth::class);
Route::put('obat/update/{id}', 'ObatController@status')->middleware(Checkauth::class);
Route::delete('obat/{id}', 'ObatController@delete')->middleware(Checkauth::class);

Route::get('purchase', 'PurchaseController@index')->middleware(Checkauth::class);
Route::get('purchase/{id}', 'PurchaseController@show')->middleware(Checkauth::class);
Route::post('purchase', 'PurchaseController@store')->middleware(Checkauth::class);
//Route::put('purchase/{id}', 'PurchaseController@update');
Route::put('purchase/update/{id}', 'PurchaseController@status');
Route::put('purchase/update_lunas/{id}', 'PurchaseController@pelunasan');
Route::delete('purchase/{id}', 'PurchaseController@delete')->middleware(Checkauth::class);

Route::get('sales', 'SalesController@index')->middleware(Checkauth::class);
Route::get('sales/{id}', 'SalesController@show')->middleware(Checkauth::class);
Route::post('sales', 'SalesController@store')->middleware(Checkauth::class);
// Route::put('sales/{id}', 'SalesController@update')->middleware(Checkauth::class);
Route::put('sales/update/{id}', 'SalesController@status');
Route::delete('sales/{id}', 'SalesController@delete')->middleware(Checkauth::class);

Route::get('retur/purchase', 'ReturController@indexPurchase')->middleware(Checkauth::class);
Route::get('retur/sales', 'ReturController@indexSales')->middleware(Checkauth::class);

Route::get('stok', 'StokController@index')->middleware(Checkauth::class);
Route::get('stok/{id}', 'StokController@show')->middleware(Checkauth::class);
Route::put('stok/{id}', 'StokController@update')->middleware(Checkauth::class);

Route::get('index_report_sales', 'ReportController@indexReportSales')->middleware(Checkauth::class);
Route::get('index_report_purchase', 'ReportController@indexReportPurchase')->middleware(Checkauth::class);
Route::get('index_report_profit', 'ReportController@indexReportProfit')->middleware(Checkauth::class);
Route::get('index_report_debt', 'ReportController@indexReportDebt')->middleware(Checkauth::class);
Route::get('index_report_debt_period', 'ReportController@indexReportDebtPeriod')->middleware(Checkauth::class);

Route::get('report/sales', 'ReportController@reportSales');//->middleware(Checkauth::class);
Route::get('report/purchase', 'ReportController@reportPurchase');//->middleware(Checkauth::class);
Route::get('report/profit', 'ReportController@reportProfit');//->middleware(Checkauth::class);
Route::get('report/debt', 'ReportController@reportDebt');//->middleware(Checkauth::class);
Route::get('report/debt/period', 'ReportController@reportDebtPeriod');//->middleware(Checkauth::class);

Route::post('register', 'Auth\RegisterController@register');

Route::get('dashboard/hutang','DashboardController@tempolist');
Route::get('dashboard/expired','DashboardController@expiredlist');
Route::get('dashboard/expiredsoon','DashboardController@expiredsoon');
Route::get('dashboard/debtsoon','DashboardController@debtsoon');

Route::post('login', 'AuthController@verify');
Route::get('profile', 'AuthController@profile')->middleware(Checkauth::class);
Route::put('profile/update', 'AuthController@update_profile')->middleware(Checkauth::class);

Route::post('logout', 'AuthController@logout');
