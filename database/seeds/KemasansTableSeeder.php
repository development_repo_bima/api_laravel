<?php

use Illuminate\Database\Seeder;
Use App\Kemasan;

class KemasansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Kemasan::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Kemasan::create([
                'nama_kemasan' 	=> $faker->sentence,
                'keterangan' 	=> $faker->paragraph,
            ]);
        }
    }
}
