<?php

use Illuminate\Database\Seeder;
Use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Role::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 10; $i++) {
            Role::create([
                'role_name' => $faker->sentence,
                'keterangan' => $faker->paragraph,
            ]);
        }
    }
}
