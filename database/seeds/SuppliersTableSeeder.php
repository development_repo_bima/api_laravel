<?php

use Illuminate\Database\Seeder;
Use App\Supplier;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Supplier::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Supplier::create([
                'nama_supplier' 	=> $faker->sentence,
                'alamat_supplier' 	=> $faker->paragraph,
                'tempo'				=> 30,
                'status'			=> 1
            ]);
        }
    }
}
