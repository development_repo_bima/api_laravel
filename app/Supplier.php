<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'm_supplier';
	protected $primaryKey = 'id_supplier';
    protected $fillable = [
    	'nama_supplier', 
    	'alamat_supplier',
    	'tempo',
    	'status'
    ];
}
