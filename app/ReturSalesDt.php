<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturSalesDt extends Model
{
    protected $table = 't_retur_sales_dt';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'id_header',
    	'id_obat',
    	'id_satuan',
    	'qty',
        'init_satuan',
    	'discount',
    	'harga'
    ];
}
