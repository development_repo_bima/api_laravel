<?php
namespace App\Helper;

use Illuminate\Support\Facades\URL;

class AppHelper{
	public static function ResponseOK(string $statusCode = '', string $str = ''){
		if($statusCode === '200'){
			$msg = "Successfully Loaded";
		}else if($statusCode === '201'){
			$msg = 'Save Data Successfully';
		}else if($statusCode === 'true'){
			$msg = $str;
		}else{
			$msg = 'Successfully';
		}
		return [
			'status'    => true,
            'msg'       => $msg,
            'currentUrl'=> URL::current()
		];
	}

	public static function FailResponse(string $statusCode = '', string $str = ''){
		
		if($statusCode === '404'){
			$msg = 'Not Found';
		}else if($statusCode === '500'){
			$msg = 'Error Server, Try Again Later';
		}else if($statusCode === 'custom'){
			$msg = $str;
		}else{
			$msg = 'it seems not loaded perfectly';
		}
		return [
			'status'    => false,
            'msg'       => $msg,
            'currentUrl'=> URL::current()
		];
	}

}