<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturPurchaseHd extends Model
{
    protected $table = 't_retur_purchase_hd';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'id_supplier',
    	'no_faktur',
    	'purchasing_date',
    	'total',
    	'ppn',
    	'grand_total',
    	'medrep',
    	'tempo'
    ];
}
