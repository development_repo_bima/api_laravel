<?php

namespace App\Http\Middleware;
use App\User;
use Closure;
use DateTime;

class Checkauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oAuth = $request->header('authorization');
        if($oAuth){
            $auth = explode(" ",$oAuth);
            if($auth[0] === 'Bearer'){
                $data = User::where('api_token',$auth[1])->firstOrFail();
                // $data = User::where('api_token', 'like', '%'.$auth[1].'%')->firstOrFail();
                // $data = User::where('api_token', $auth[1])->get();
                $datafresh = $data->fresh();
                

                if(!$data){
                    $data = $datafresh;
                }

                if($data){
                    $valid = User::find($data->id);
                    /*$now = date("g:i a", strtotime("now"));
                    $expired = date("g:i a", strtotime($valid->valid_until));*/

                    $now = new DateTime("now");
                    $expired = new DateTime($valid->valid_until);

                    $obj_diff = $now->diff($expired);

                    $diff = $obj_diff->days * 24 * 60;
                    $diff += $obj_diff->h * 60;
                    $diff += $obj_diff->i;

                    // print_r($diff);
                    // print_r($obj_diff->invert);die;
                    if(!$obj_diff->invert && $diff){
                        $newtimestamp = strtotime($valid->valid_until.' + 5 minute');
                        $valid->valid_until = date('Y-m-d H:i:s', $newtimestamp);
                        $valid->save();

                        /*print_r($expired);die;*/

                        return $next($request);
                    }else{
                        $valid->api_token = null;
                        $valid->valid_until = null;
                        $valid->save();
                        return response()->json([
                            'status'    => false,
                            'flag'      => 1,
                            'msg'       => 'Session has expired! Please Re-Login'
                        ], 401);
                    }
                }else{
                    return response()->json([
                        'status'    => false,
                        'flag'      => 1,
                        'msg'       => 'User Not Found'
                    ], 404);
                }
            }else{
                return response()->json([
                    'status'    => false,
                    'flag'      => 1,
                    'msg'       => 'Invalid Headers Schema'
                ], 401);
            }
        }else{
            return response()->json([
                'status'    => false,
                'flag'      => 1,
                'msg'       => "You don't have permission to access this api"
            ], 401);
        }
    }
}
