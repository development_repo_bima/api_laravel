<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Supplier;
use AppHelper;
use Validator;

class SupplierController extends Controller
{
    public function index(Request $request)
    {

        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('m_supplier');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('alamat_supplier','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->orderBy('nama_supplier', 'asc')
                    ->get()
                );
        
        if($total_rows){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
    }
 
    public function show($id)
    {
        $data = Supplier::find($id, ['id_supplier','nama_supplier','alamat_supplier','tempo','status']);
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function store(Request $request)
    {
        if($this->_validate($request->all()) === true){
            if(Supplier::create($request->all())){
                return response()->json(AppHelper::ResponseOK('201'),201);    
            }else{
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }

    public function update(Request $request, $id)
    {
        if($this->_validate($request->all()) === true){
            $supplier = Supplier::findOrFail($id);
            if($supplier->update($request->all())){
                return response()->json(AppHelper::ResponseOK(),200);
            }else{
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }

    public function status(Request $request, $id){
        $supplier = Supplier::findOrFail($id);
        if($supplier->update($request->all())){
            return response()->json(AppHelper::ResponseOK(),200);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    }

    public function delete(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        if($supplier->delete()){
            return response()->json(AppHelper::ResponseOK(),204);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    }

    public function get_supplier($id = null){
        $data = Supplier::all(['id_supplier AS value','nama_supplier AS label']);
        
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function _validate($request){
        $validator = Validator::make($request, [
            'nama_supplier'  => 'required',
            'alamat_supplier'=> 'required',
            'tempo'          => 'required',
            'status'         => 'required',
        ],
        [
            'required'  => 'Kolom :attribute harus diisi'
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
            $msg = ''; $x = 1;
            
            foreach ($errors->all() as $message) {
                $msg .= $message;
                if($x < count($errors->all())){
                    $msg .= '<br>,';
                }else{
                    $msg .= '.';
                }
                $x++;  
            }
            return $msg;   
        }else{
            return true;
        }
    }
}
