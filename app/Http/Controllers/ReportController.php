<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use AppHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Obat;

class ReportController extends Controller
{
	public function indexReportSales(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_sales_dt AS a')
        	->leftJoin('t_sales_hd AS b', 'a.id_header','=','b.id')
        	->leftJoin('m_obat AS c', 'a.id_obat','=','c.id_obat')
        	->leftJoin('m_kemasan AS d', 'a.id_satuan','=','d.id_kemasan');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('c.nama_obat','like','%'.$param['search'].'%')
                ->orWhere('b.no_order','like','%'.$param['search'].'%');
        }

        if(isset($param['query']) && $param['query']){
        	$decode = json_decode(base64_decode($param['query']), true);
        	$data->whereBetween('b.tgl_order', [$decode['date_from'], $decode['date_to']]);
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.no_order', 'b.tgl_order', 'c.nama_obat', 'd.nama_kemasan', 'a.qty','a.discount', 'a.harga')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function indexReportPurchase(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_purchase_dt AS a')
    		->leftJoin('t_purchase_hd AS b', 'a.id_header','=','b.id')
    		->leftJoin('m_supplier AS c', 'c.id_supplier','=','b.id_supplier')
    		->leftJoin('m_obat AS d','d.id_obat','=','a.id_obat')
    		->leftJoin('m_kemasan AS e','e.id_kemasan','=','a.id_satuan');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('b.no_faktur','like','%'.$param['search'].'%')
                ->orWhere('c.nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('a.batch','like','%'.$param['search'].'%')
                ->orWhere('d.nama_obat','like','%'.$param['search'].'%');
        }

        if(isset($param['query']) && $param['query']){
        	$decode = json_decode(base64_decode($param['query']), true);
        	$data->whereBetween('b.purchasing_date', [$decode['date_from'], $decode['date_to']]);
        }
        
        $total_rows = $data->count();

        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.no_faktur', 'c.nama_supplier', 'b.purchasing_date','b.medrep', 'b.tempo', 'b.flag_retur','d.nama_obat', 'e.nama_kemasan', 'a.batch', 'a.expired_date', 'a.diskon','a.qty','a.harga')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function indexReportDebtPeriod(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];

        $data = DB::table('t_purchase_hd AS a')
            ->leftJoin('m_supplier AS c','c.id_supplier','=','a.id_supplier')
            ->where('a.tempo_date','<>',null)
            ->where('a.tempo','>',0)
            ->where('a.flag_lunas','=',0)
            ->where('a.flag_retur','=',0)
            ->groupBy(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'));

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_faktur','like','%'.$param['search'].'%')
                ->orWhere('c.nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('d.nama_obat','like','%'.$param['search'].'%');
        }

        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);
            $data->whereBetween(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']]);
        }
        
        $total_rows = $data->count();

        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m") as period'), DB::raw('SUM(a.grand_total) as total'))
                    ->get()
                );

        $from = substr($decode['date_from'],-2);
        $to = substr($decode['date_to'],-2);
        $year_from = substr($decode['date_from'],0,4);
        $year_to = substr($decode['date_to'],0,4);
        
        foreach($result as $k=>$v){
            
            $p = substr($v->period, -2);

            $arr[(int)$p] = [
                'period'    => $v->period,
                'total'     => $v->total
            ];
        }

        for($i=(int)$from; $i<=(int)$to; $i++){

            $str = (strlen($i) === 1)? '0'.$i : $i;
            $res[] = [
                'period'    => $year_to.'-'.$str,
                'total'     => (isset($arr[$i]['total']) && $arr[$i]['total']) ? $arr[$i]['total'] : 0
            ];
        }

        if($res){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $res,
                'total'     => count($res),
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function indexReportDebt(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];

        $data = DB::table('t_purchase_hd AS a')
            ->leftJoin('m_supplier AS c','c.id_supplier','=','a.id_supplier')
            ->where('a.tempo_date','<>',null)
            ->where('a.tempo','>',0)
            ->where('a.flag_lunas','=',0)
            ->where('a.flag_retur','=',0);

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_faktur','like','%'.$param['search'].'%')
                ->orWhere('c.nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('d.nama_obat','like','%'.$param['search'].'%');
        }

        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);
            // print_r($decode);die;
            // $data->whereBetween(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']]);
            $data->where(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), '=', $decode);
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'c.nama_supplier')
                    ->get()
                );

        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function indexReportProfit(Request $request){
        $param = $request->all();
        
        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);
            
            // $data->whereBetween('b.purchasing_date', [$decode['date_from'], $decode['date_to']]);

            $purchase_tempo = DB::table('t_purchase_hd')
                ->where('tempo','>',0)
                ->where('flag_lunas','=',1)
                ->where('flag_retur','=',0)
                ->groupBy(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'))
                // ->sum('grand_total')
                ->whereBetween(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                ->select(DB::raw('SUM(grand_total) AS total'), DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m") AS purchase'))
                ->get()->all();

            $purchase_cod = DB::table('t_purchase_hd')
                    ->where('tempo','=',null)
                    ->where('flag_retur','=',0)
                    ->groupBy(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'))
                    ->whereBetween(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                    ->select(DB::raw('SUM(grand_total) AS total'), DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m") AS purchase'))
                    ->get()->all();
            
            $sales = DB::table('t_sales_dt AS a')
                    ->leftJoin('t_sales_hd AS b', 'a.id_header','=','b.id')
                    ->where('b.flag_retur','=',0)
                    ->groupBy(DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m")'))
                    ->whereBetween(DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                    ->select(DB::raw('SUM(a.harga) AS total'), DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m") AS sales_date'))
                    ->get()->all();
            
            $from = substr($decode['date_from'],-2);
            $to = substr($decode['date_to'],-2);
            $year_from = substr($decode['date_from'],0,4);
            $year_to = substr($decode['date_to'],0,4);
            
            foreach($purchase_tempo as $k=>$v){
                
                $p = substr($v->purchase, -2);

                $arr_tempo[(int)$p] = [
                    'period'    => $v->purchase,
                    'total'     => $v->total
                ];
            }
            
            foreach($purchase_cod as $k=>$v){
                
                $p = substr($v->purchase, -2);

                $arr_cod[(int)$p] = [
                    'period'    => $v->purchase,
                    'total'     => $v->total
                ];
            }

            foreach($sales as $k=>$v){
                
                $p = substr($v->sales_date, -2);

                $arr_sales[(int)$p] = [
                    'period'    => $v->sales_date,
                    'total'     => $v->total
                ];
            }
            
            for($i=(int)$from; $i<=(int)$to; $i++){

                $str = (strlen($i) === 1)? '0'.$i : $i;
                $tempo = (isset($arr_tempo[$i]['total']) && $arr_tempo[$i]['total']) ? $arr_tempo[$i]['total'] : 0;
                $cod = (isset($arr_cod[$i]['total']) && $arr_cod[$i]['total']) ? $arr_cod[$i]['total'] : 0;
                $sales = (isset($arr_sales[$i]['total']) && $arr_sales[$i]['total']) ? $arr_sales[$i]['total'] : 0;
                $res[] = [
                    'period'        => $year_to.'-'.$str,
                    'pembelian'     => ($tempo + $cod),
                    'penjualan'     => $sales
                ];
            }

            if($res){
                return response()->json(array_merge(AppHelper::ResponseOK(), [
                    'data'      => $res,
                    'total'     => count($res),
                ]), 200);    
            }else{
                return response()->json(array_merge(AppHelper::FailResponse('404'), [
                    'data'      => [],
                    'total'     => 0,
                ]), 404);
            }
        }
    }

    public function reportSales(Request $request){

    	$param = $request->all();

        $data = DB::table('t_sales_dt AS a')
        	->leftJoin('t_sales_hd AS b', 'a.id_header','=','b.id')
        	->leftJoin('m_obat AS c', 'a.id_obat','=','c.id_obat')
        	->leftJoin('m_kemasan AS d', 'a.id_satuan','=','d.id_kemasan');

        if(isset($param['query']) && $param['query']){
        	$decode = json_decode(base64_decode($param['query']), true);
        	$data->whereBetween('b.tgl_order', [$decode['date_from'], $decode['date_to']]);
        }

    	$data = $data->select('b.no_order', 'b.tgl_order', DB::raw("CONCAT(c.nama_obat,' (',if(nama_pasien, 'Racikan' , 'Non Racikan'), ')') AS nama_obat"), 'd.nama_kemasan', DB::raw("CONCAT(a.qty,' ', d.nama_kemasan) AS qty"),'a.discount', 'a.harga')
        	->get();
        
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function reportPurchase(Request $request){

    	$param = $request->all();

    	$data = DB::table('t_purchase_dt AS a')
    		->leftJoin('t_purchase_hd AS b', 'a.id_header','=','b.id')
    		->leftJoin('m_supplier AS c', 'c.id_supplier','=','b.id_supplier')
    		->leftJoin('m_obat AS d','d.id_obat','=','a.id_obat')
    		->leftJoin('m_kemasan AS e','e.id_kemasan','=','a.id_satuan');

    	if(isset($param['query']) && $param['query']){
        	$decode = json_decode(base64_decode($param['query']), true);
        	$data->whereBetween('b.purchasing_date', [$decode['date_from'], $decode['date_to']]);
        }

		$data = $data->select('b.no_faktur', 'c.nama_supplier', 'b.purchasing_date','b.medrep', 'b.tempo', 'b.flag_retur', 'b.flag_lunas','d.nama_obat', 'e.nama_kemasan', 'a.batch', 'a.expired_date', 'a.diskon','a.qty','a.harga')
    		->get()->all();

		if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function reportDebt(Request $request){
        $param = $request->all();

        $data = DB::table('t_purchase_hd AS a')
            ->leftJoin('m_supplier AS c','c.id_supplier','=','a.id_supplier')
            ->where('a.tempo_date','<>',null)
            ->where('a.tempo','>',0)
            ->where('a.flag_lunas','=',0)
            ->where('a.flag_retur','=',0);

        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);

            // $data->whereBetween(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']]);
            $data->where(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), '=', $decode);
        }

        $data = $data->select('a.*', 'c.nama_supplier')
            ->get()->all();
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function reportDebtPeriod(Request $request){
        $param = $request->all();

        $data = DB::table('t_purchase_hd AS a')
            ->leftJoin('m_supplier AS c','c.id_supplier','=','a.id_supplier')
            ->where('a.tempo_date','<>',null)
            ->where('a.tempo','>',0)
            ->where('a.flag_lunas','=',0)
            ->where('a.flag_retur','=',0)
            ->groupBy(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'));;

        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);
            
            $data->whereBetween(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']]);
        }

        $data = $data->select(DB::raw('DATE_FORMAT(a.purchasing_date,"%Y-%m") as period'), DB::raw('SUM(a.grand_total) as total'))
            ->get()->all();

        $from = substr($decode['date_from'],-2);
        $to = substr($decode['date_to'],-2);
        $year_from = substr($decode['date_from'],0,4);
        $year_to = substr($decode['date_to'],0,4);
        
        foreach($data as $k=>$v){
            
            $p = substr($v->period, -2);

            $arr[(int)$p] = [
                'period'    => $v->period,
                'total'     => $v->total
            ];
        }

        for($i=(int)$from; $i<=(int)$to; $i++){

            $str = (strlen($i) === 1)? '0'.$i : $i;
            $res[] = [
                'period'    => $year_to.'-'.$str,
                'total'     => (isset($arr[$i]['total']) && $arr[$i]['total']) ? $arr[$i]['total'] : 0
            ];
        }

        if($res){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $res,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function reportProfit(Request $request){
        $param = $request->all();

        if(isset($param['query']) && $param['query']){
            $decode = json_decode(base64_decode($param['query']), true);
            
            // $data->whereBetween('b.purchasing_date', [$decode['date_from'], $decode['date_to']]);

            $purchase_tempo = DB::table('t_purchase_hd')
                ->where('tempo','>',0)
                ->where('flag_lunas','=',1)
                ->where('flag_retur','=',0)
                ->groupBy(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'))
                // ->sum('grand_total')
                ->whereBetween(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                ->select(DB::raw('SUM(grand_total) AS total'), DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m") AS purchase'))
                ->get()->all();

            $purchase_cod = DB::table('t_purchase_hd')
                    ->where('tempo','=',null)
                    ->where('flag_retur','=',0)
                    ->groupBy(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'))
                    ->whereBetween(DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                    ->select(DB::raw('SUM(grand_total) AS total'), DB::raw('DATE_FORMAT(purchasing_date,"%Y-%m") AS purchase'))
                    ->get()->all();
            
            $sales = DB::table('t_sales_dt AS a')
                    ->leftJoin('t_sales_hd AS b', 'a.id_header','=','b.id')
                    ->where('b.flag_retur','=',0)
                    ->groupBy(DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m")'))
                    ->whereBetween(DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m")'), [$decode['date_from'], $decode['date_to']])
                    ->select(DB::raw('SUM(a.harga) AS total'), DB::raw('DATE_FORMAT(b.tgl_order,"%Y-%m") AS sales_date'))
                    ->get()->all();
            
            $from = substr($decode['date_from'],-2);
            $to = substr($decode['date_to'],-2);
            $year_from = substr($decode['date_from'],0,4);
            $year_to = substr($decode['date_to'],0,4);
            
            foreach($purchase_tempo as $k=>$v){
                
                $p = substr($v->purchase, -2);

                $arr_tempo[(int)$p] = [
                    'period'    => $v->purchase,
                    'total'     => $v->total
                ];
            }
            
            foreach($purchase_cod as $k=>$v){
                
                $p = substr($v->purchase, -2);

                $arr_cod[(int)$p] = [
                    'period'    => $v->purchase,
                    'total'     => $v->total
                ];
            }

            foreach($sales as $k=>$v){
                
                $p = substr($v->sales_date, -2);

                $arr_sales[(int)$p] = [
                    'period'    => $v->sales_date,
                    'total'     => $v->total
                ];
            }
            
            for($i=(int)$from; $i<=(int)$to; $i++){

                $str = (strlen($i) === 1)? '0'.$i : $i;
                $tempo = (isset($arr_tempo[$i]['total']) && $arr_tempo[$i]['total']) ? $arr_tempo[$i]['total'] : 0;
                $cod = (isset($arr_cod[$i]['total']) && $arr_cod[$i]['total']) ? $arr_cod[$i]['total'] : 0;
                $sales = (isset($arr_sales[$i]['total']) && $arr_sales[$i]['total']) ? $arr_sales[$i]['total'] : 0;
                $res[] = [
                    'period'        => $year_to.'-'.$str,
                    'pembelian'     => ($tempo + $cod),
                    'penjualan'     => $sales
                ];
            }

            if($res){
                return response()->json(array_merge(AppHelper::ResponseOK(),[
                    'data'=> $res,
                ]),200);
            }else{
                return response()->json(array_merge(AppHelper::FailResponse('404'),[
                    'data'=> []
                ]),404);
            }
        }

        
    }
}
