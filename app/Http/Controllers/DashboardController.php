<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use AppHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;


class DashboardController extends Controller
{
    public function tempolist(){
    	
    	$data = DB::table('t_purchase_hd AS a')
    		->leftJoin('m_supplier AS c', 'c.id_supplier','=','a.id_supplier')
    		->whereBetween('a.tempo_date', [date('Y-m-01',strtotime('this month')), date('Y-m-t',strtotime('this month'))])
    		->where('flag_lunas','0')
    		->where('flag_retur','0')
    		->select('a.*', 'c.nama_supplier')
            ->orderBy('a.id', 'desc')->get()->all();
    	
    	return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);

    }

    public function expiredlist(){
    	$data = DB::table('t_stok AS a')
    		->leftJoin('m_obat AS c', 'a.id_obat','=','c.id_obat')
        	->leftJoin('m_kemasan AS d', 'c.satuan_kecil','=','d.id_kemasan')
        	->whereBetween('a.expired_date', [date('Y-m-01',strtotime('this month')), date('Y-m-t',strtotime('this month'))])
        	->select('a.*', 'c.nama_obat', 'd.nama_kemasan')
            ->orderBy('a.id', 'desc')->get()->all();

        return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
    }

    public function expiredsoon(){
        $backdate = strtotime("+2 day");
        $tempo = date('Y-m-d',$backdate);

        $data = DB::table('t_stok AS a')
            ->leftJoin('m_obat AS c', 'a.id_obat','=','c.id_obat')
            ->leftJoin('m_kemasan AS d', 'c.satuan_kecil','=','d.id_kemasan')
            ->whereBetween('a.expired_date', [date('Y-m-d'), $tempo])
            ->select('a.*', 'c.nama_obat', 'd.nama_kemasan')
            ->orderBy('a.id', 'asc')->get()->all();

        return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
    }

    public function debtsoon(){
        $backdate = strtotime("+2 day");
        $tempo = date('Y-m-d',$backdate);

        $data = DB::table('t_purchase_hd AS a')
            ->leftJoin('m_supplier AS c', 'c.id_supplier','=','a.id_supplier')
            ->whereBetween('a.tempo_date', [date('Y-m-d'), $tempo])
            ->where('flag_lunas','0')
            ->where('flag_retur','0')
            ->select('a.*', 'c.nama_supplier')
            ->orderBy('a.id', 'asc')->get()->all();
        
        return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
    }
}
