<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use AppHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Obat;
use App\Stok;

class ObatController extends Controller
{
    public function index(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('m_obat AS a')
                ->leftJoin('m_kemasan AS b', 'a.satuan_besar', '=', 'b.id_kemasan')
                ->leftJoin('m_kemasan AS c', 'a.satuan_mid', '=', 'c.id_kemasan')
                ->leftJoin('m_kemasan AS d', 'a.satuan_kecil', '=', 'd.id_kemasan');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('nama_obat','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.nama_kemasan AS kemasan_besar','c.nama_kemasan AS kemasan_mid','d.nama_kemasan AS kemasan_kecil')
                    ->orderBy('a.nama_obat', 'asc')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }
 
    public function show($id)
    {
        $data = Obat::find($id, ['id_obat','nama_obat','margin','harga_net', 'harga_jual', 'satuan_besar','satuan_kecil','isi_satuan_kecil']);
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
    	}else{
	        return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
    	}
        
    }

    public function store(Request $request)
    {
    	
        if($this->_validate($request->all()) === true){
            DB::beginTransaction();
            
            if($id_obat = Obat::create($request->all())->id_obat){

                $flag = true;
                $input = $request->all();
                if(isset($input['has_stok']) && $input['has_stok']){

                    $mid = (isset($input['isi_satuan_mid']) && $input['isi_satuan_mid']) ? $input['isi_satuan_mid'] : 1;
                    $kecil = ($input['isi_satuan_kecil']) ? $input['isi_satuan_kecil'] : 1;
                    $stock = $input['stok_awal'];

                    $stok = new Stok;

                    $stok->id_obat = $id_obat;
                    $stok->satuan_besar = 1;
                    $stok->satuan_mid = $mid;
                    $stok->satuan_kecil = $kecil;
                    $stok->stok = $stock;
                    $stok->stok_awal = 0;
                    
                    $flag = $stok->save();
                }
                    
                if(!$flag){
                    DB::rollback();
                    return response()->json(AppHelper::FailResponse('500'),500);
                }else{
                    DB::commit();
                    return response()->json(AppHelper::ResponseOK('201'),201);
                }
            }else{
                DB::rollback();
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }

    public function update(Request $request, $id)
    {
        if($this->_validate($request->all()) === true){
            $obat = Obat::findOrFail($id);
            if($obat->update($request->all())){
                $flag = true;
                $input = $request->all();
                if(isset($input['has_stok']) && $input['has_stok']){

                    $dt = Stok::where('id_obat', $id)->first();
                        
                    if(!$dt){
                        $mid = (isset($input['isi_satuan_mid']) && $input['isi_satuan_mid']) ? $input['isi_satuan_mid'] : 1;
                        $kecil = ($input['isi_satuan_kecil']) ? $input['isi_satuan_kecil'] : 1;
                        $stock = $input['stok_awal'];

                        $stok = new Stok;

                        $stok->id_obat = $id;
                        $stok->satuan_besar = 1;
                        $stok->satuan_mid = $mid;
                        $stok->satuan_kecil = $kecil;
                        $stok->stok = $stock;
                        $stok->stok_awal = 0;
                        
                        $flag = $stok->save();
                    }
                    
                }
                return response()->json(AppHelper::ResponseOK(),201);
            }else{
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }

    public function status(Request $request, $id){
        $supplier = Obat::findOrFail($id);
        if($supplier->update($request->all())){
            return response()->json(AppHelper::ResponseOK(),200);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    }

    public function get_obat(){
        /*$data = Obat::all(['id_obat AS value','nama_obat AS text', 'satuan_besar','satuan_mid','satuan_kecil']);
*/
        $data = DB::table('m_obat AS a')
            ->leftJoin('m_kemasan AS b', 'a.satuan_besar', '=', 'b.id_kemasan')
            ->leftJoin('m_kemasan AS c', 'a.satuan_mid', '=', 'c.id_kemasan')
            ->leftJoin('m_kemasan AS d', 'a.satuan_kecil', '=', 'd.id_kemasan')
            ->select('a.id_obat AS value','a.nama_obat AS label', 'a.harga_net', 'a.harga_jual', 'satuan_besar', 'satuan_mid','satuan_kecil', 'a.satuan_besar','a.satuan_mid','a.satuan_kecil', 'a.isi_satuan_mid', 'a.isi_satuan_kecil', 'b.nama_kemasan AS kemasan_besar','c.nama_kemasan AS kemasan_mid','d.nama_kemasan AS kemasan_kecil')
            ->get();
        
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function delete(Request $request, $id)
    {
        $obat = Obat::findOrFail($id);
        $stok = Stok::where('id_obat', $id);
        
        if($obat->delete() && $stok->delete()){
            return response()->json(AppHelper::ResponseOK(),204);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    }

    public function _validate($request){
        $validator = Validator::make($request, [
            'nama_obat'         => 'required',
            'satuan_besar'      => 'required',
            'isi_satuan_kecil'  => 'required',
            'satuan_kecil'      => 'required'
        ],
        [
            'required'  => 'Kolom :attribute harus diisi'
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
            $msg = ''; $x = 1;
            
            foreach ($errors->all() as $message) {
                $msg .= $message;
                if($x < count($errors->all())){
                    $msg .= '<br>,';
                }else{
                    $msg .= '.';
                }
                $x++;  
            }
            return $msg;   
        }else{
            return true;
        }
    }
}
