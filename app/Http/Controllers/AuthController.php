<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use AppHelper;
use App\User;

class AuthController extends Controller
{
    public function verify(Request $request){
        
        $params = $request->input('data');
        $data = User::where('email',$params['email'])->first();
        
        if($data){
        	if(isset($params['password']) && Hash::check($params['password'], $data->password)){
	        	$valid = User::find($data->id);
	        	$valid->api_token = str_random(60);
	        	$newtimestamp = strtotime("now".' + 15 minute');
                $valid->valid_until = date('Y-m-d H:i:s', $newtimestamp);
                
	        	$valid->save();

	        	return response()->json([
		            'status'    => true,
		            'msg'		=> 'Berhasil Login..',
		            'token'     => $valid->api_token,
                    'role'      => $valid->role
		        ], 200);
        	}else{
        		return response()->json([
		            'status'    => false,
		            'msg'		=> 'Password mismatch',
		            'token'     => null
		        ], 404);
        	}
        }else{
        	return response()->json([
		            'status'    => false,
		            'msg'		=> 'User Not found',
		            'token'     => null
		        ], 404);
        }

    }

    public function logout(Request $request){
    	
    	$oAuth = $request->header('authorization');
    	if($oAuth){
            $auth = explode(" ",$oAuth);
            if($auth[0] === 'Bearer'){
                $data = User::where('api_token',$auth[1])->first();
                if($data){
                    $valid = User::find($data->id);
                    $valid->api_token = null;
                    $valid->valid_until = null;
                    $valid->save();

                    return response()->json([
                        'status'    => true,
                        'msg'       => 'You have Signed Out'
                    ], 200);
                }else{
                    return response()->json([
                        'status'    => false,
                        'msg'       => 'User Not Found'
                    ], 404);
                }
            }else{
                return response()->json([
                    'status'    => false,
                    'msg'       => 'Invalid Headers Schema'
                ], 401);
            }
        }else{
            return response()->json([
                'status'    => false,
                'msg'       => "You don't have permission to access this api"
            ], 401);
        }
    }

    public function profile(Request $request){
        $oAuth = $request->header('authorization');
        if($oAuth){
            $auth = explode(" ",$oAuth);
            $data = User::where('api_token',$auth[1])->firstOrFail();
            $datafresh = $data->fresh();
            
            if(!$data){
                $data = $datafresh;
            }

            if($data){
                $valid = User::find($data->id);

                return response()->json(array_merge(AppHelper::ResponseOK(), [
                    'data'      => $valid
                ]), 200); 
            }else{
                return response()->json([
                    'status'    => false,
                    'flag'      => 1,
                    'msg'       => 'User Not Found'
                ], 404);
            }
        }else{
            return response()->json([
                'status'    => false,
                'flag'      => 1,
                'msg'       => "You don't have permission to access this api"
            ], 401);
        }
    }

    public function update_profile(Request $request){
        $input = $request->all();

        $update = User::find($input['id']);

        if(isset($input['otentifikasi']) && Hash::check($input['otentifikasi'], $update->password)){
            
            $update->email = $input['email'];            
            $update->name = $input['name']; 

            if($input['change_password']){
                $update->unhash = $input['new_password'];
                $update->password = Hash::make($input['new_password']);
            }

            $update->update();
            return response()->json(AppHelper::ResponseOK('201'),200);          
        }else{
            return response()->json([
                'status'    => false,
                'msg'       => 'Challenge Password Failed !'
            ], 404);
        }
        


    }
}
