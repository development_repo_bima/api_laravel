<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use AppHelper;
use App\Stok;
use App\SalesHd;
use App\SalesDt;
use App\ReturSalesHd;
use App\ReturSalesDt;

class SalesController extends Controller
{
    public function index(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_sales_hd AS a');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_order','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->orderBy('a.id', 'desc')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function show($id)
    {
        $hd = SalesHd::find($id, [
            'no_order',
            'tgl_order',
            'no_resep',
            'nama_pasien',
            'keterangan_resep',
            'flag_retur'
        ]);

        $dt = DB::table('t_sales_dt AS a')
                ->where('a.id_header','=',$id)
                ->leftJoin('m_obat AS b','a.id_obat','=','b.id_obat')
                ->select('a.id_header',
            'a.id_obat',
            'b.nama_obat',
            'a.id_satuan',
            'a.qty',
            'a.init_satuan',
            'a.discount',
            'a.harga')->get();

        if($hd && $dt){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'header'=> $hd,
                'detail'=> $dt,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'header'=> [],
                'detail'=> [],
            ]),404);
        }
    }

    public function status(Request $request, $id){
        $hd = SalesHd::findOrFail($id);
    	$dt = SalesDt::where('id_header', $id)->get();

    	if($hd){
    		$returhd = new ReturSalesHd;
    		
    		$returhd->no_order = $hd->no_order;
	    	$returhd->tgl_order = $hd->tgl_order;
	    	$returhd->nama_pasien = $hd->nama_pasien;
	    	$returhd->keterangan_resep = $hd->keterangan_resep;
	    	$returhd->no_resep = $hd->no_resep;
	    	DB::beginTransaction();
	    	if($dt && $returhd->save()){
	    		foreach ($dt as $v) {
	    			$returdt = new ReturSalesDt;

	    			$returdt->id_header = $returhd->id;
			    	$returdt->id_obat = $v->id_obat;
			    	$returdt->qty = $v->qty;
			    	$returdt->id_satuan = $v->id_satuan;
			    	$returdt->harga = $v->harga;
			    	$returdt->discount = $v->discount;

			    	if(!$returdt->save()){
			    		$del = ReturSalesHd::find($returhd->id);
			    		$del->delete();
			    		DB::rollback();
			    		return response()->json(AppHelper::FailResponse('500'), 500);
			    	}

                    $stock = Stok::where('id_obat', $v->id_obat)->first();
                    if($stock){
                        $mid = ($stock->satuan_mid) ? $stock->satuan_mid : 1;
                        $kecil = ($stock->satuan_kecil) ? $stock->satuan_kecil : 1;

                        if($v->init_satuan === 'B'){
                            $stok = $v->qty * $mid * $kecil;
                        }else if($v->init_satuan === 'S'){
                            $stok = $v->qty * $kecil;
                        }else if($v->init_satuan === 'K'){
                            $stok = $v->qty;
                        }
                        $update = [
                            'id_last_keluar'=>$returhd->id,
                            'stok'  => ($stock->stok + $stok),
                        ];

                        $stock->update($update);
                    }
	    		}
	    		$hd->update(['flag_retur'=>1]);
	    		DB::commit();
	    		return response()->json(AppHelper::ResponseOK('201'),201); 
	    	}else{
	    		return response()->json(AppHelper::FailResponse('500'), 500);
	    	}
    	}else{
    		return response()->json(AppHelper::FailResponse('404'), 404);
    	}
    }

 	public function store(Request $request)
    {
    	$input = $request->all();
        
        if($this->_validate($input) === true){
        	DB::beginTransaction();
        	try{
                $input['header']['no_order'] = $this->_count_sales();
                $input['header']['flag_retur'] = 0;

        		if($id_header = SalesHd::create($input['header'])->id){
	        		for($i=0;$i<count($input['details']);$i++){
	        			$input['details'][$i]['ppn'] = $input['details'][$i]['harga'] * 0.1;
                        $input['details'][$i]['id_header'] = $id_header;
	        			$input['details'][$i]['id_obat'] = $input['details'][$i]['id_obat']['value'];
	        			if(!SalesDt::create($input['details'][$i])){
	        				DB::rollback();
	        				$flag = false;
	        				break;
	        				return response()->json(AppHelper::FailResponse('500'),500);
	        			}

                        $dt = Stok::where('id_obat', $input['details'][$i]['id_obat'])->first();
                        if($dt){
                            $mid = ($dt->satuan_mid) ? $dt->satuan_mid : 1;
                            $kecil = ($dt->satuan_kecil) ? $dt->satuan_kecil : 1;

                            if($input['details'][$i]['init_satuan'] === 'B'){
                                $stok = $input['details'][$i]['qty'] * $mid * $kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'S'){
                                $stok = $input['details'][$i]['qty'] * $kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'K'){
                                $stok = $input['details'][$i]['qty'];
                            }
                            $update = [
                                'id_last_keluar'=>$id_header,
                                'stok'  => ($dt->stok - $stok),
                            ];

                            $dt->update($update);
                        }
	        		}
	        		$flag = true;
	        	}else{
	        		$flag = false;
	        	}	
        	} catch(\Exception $e){
        		DB::rollback();
        	}
        	
        	if($flag){
        		DB::commit();
                return response()->json(AppHelper::ResponseOK('201'),201);    
            }else{
            	DB::rollback();
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }  

    public function delete($id){
    	$hd = SalesHd::findOrFail($id);
        $details = SalesDt::where('id_header', $id)->get();
        DB::beginTransaction();
        if($hd->delete()){
            foreach($details as $v){
                $dt = Stok::where('id_obat', $v->id_obat)->first();
                if($dt){
                    $mid = ($dt->satuan_mid) ? $dt->satuan_mid : 1;
                    $kecil = ($dt->satuan_kecil) ? $dt->satuan_kecil : 1;

                    if($v->init_satuan === 'B'){
                        $stok = $v->qty * $dt->satuan_mid * $dt->satuan_kecil;
                    }else if($v->init_satuan === 'S'){
                        $stok = $v->qty * $dt->satuan_kecil;
                    }else if($v->init_satuan === 'K'){
                        $stok = $v->qty;
                    }
                    $update = [
                        //'id_last_keluar'=>$returhd->id,
                        'stok'  => ($dt->stok + $stok),
                    ];

                    $dt->update($update);
                }
            }
            $delete = SalesDt::where('id_header', $id)->delete();
            if($delete){
                DB::commit();
                return response()->json(AppHelper::ResponseOK(),204);
            }else{
                DB::rollback();
                return response()->json(AppHelper::FailResponse('500'), 500);
            }
        }else{
            DB::rollback();
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    } 

    public function _count_sales(){

        $x = 4;
        $prefix = 'S-';
        $find = SalesHd::where('tgl_order', date('Y-m-d'))->count();

        $zerofill = '';
        for($y=0;$y<($x-strlen($find));$y++){
            $zerofill .= '0';
        }

        $find++;

        return $prefix.date('Ymd').$zerofill.$find;
    }

    public function _validate($request){
    	$requestHeader = $request['header'];
    	$requestDetails = $request['details'];
    	
        $validator = Validator::make($requestHeader, [
            'no_order'       => 'required',
            'tgl_order'         => 'required',
        ],
        [
            'required'  => 'Kolom :attribute harus diisi'
        ]);

        $validator->setAttributeNames([
        	'no_order'	 => 'Nomor Order',
        	'tgl_order'	     => 'Tgl Penjualan'
        ]);

        $msg = '';
        if($validator->fails()){
            $errors = $validator->errors();
             $x = 1;
            
            foreach ($errors->all() as $message) {
                $msg .= $message;
                if($x < count($errors->all())){
                    $msg .= '<br>,';
                }else{
                    $msg .= '.';
                }
                $x++;  
            }   
            $flag = false;
        }else{
        	$flag = true;
        }
        if($requestDetails){
        	for($i=0; $i<count($requestDetails); $i++){
	        	$validatorDetails = Validator::make($requestDetails[0], [
		            'id_obat'       => 'required',
		            'id_satuan'     => 'required',
		            'qty'   		=> 'required',
		            'harga'   		=> 'required'
		        ],
		        [
		            'required'  => 'Kolom :attribute harus diisi'
		        ]);

		        $validatorDetails->setAttributeNames([
		        	'id_obat'	=> 'Nama Barang',
		        	'id_satuan'	=> 'Satuan Barang',
		        	'qty'=> 'Quantity',
		        	'harga'=> 'Harga',
		        ]);

	        	if($validatorDetails->fails()){
	        		$errors = $validatorDetails->errors();
		            $y = 1;
		            
		            foreach ($errors->all() as $message) {
		                $msg .= $message;
		                if($y < count($errors->all())){
		                    $msg .= '<br>,';
		                }else{
		                    $msg .= '.';
		                }
		                $y++;  
		            }
		            $flag = false;
	        	}else{
	        		$flag = true;
	        	}
	        }
        }else{
        	$msg .= '<br> Data barang tidak boleh kosong.';
        }
    	
        
        if($flag){
        	return true;
        }else{
        	return $msg;
        }
        
    }
}
