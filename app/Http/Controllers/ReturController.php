<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use AppHelper;

class ReturController extends Controller
{
    public function indexPurchase(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_retur_purchase_hd AS a')->join('m_supplier AS b', 'b.id_supplier', '=', 'a.id_supplier');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_faktur','like','%'.$param['search'].'%')
                ->orWhere('b.nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('a.medrep','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.nama_supplier')
                    ->orderBy('a.id', 'desc')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function indexSales(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_retur_sales_hd AS a');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_order','like','%'.$param['search'].'%');
        }
        
        $total_rows = $data->count();

        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->orderBy('a.id', 'desc')
                    // ->select('a.*', 'b.nama_supplier')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }
}
