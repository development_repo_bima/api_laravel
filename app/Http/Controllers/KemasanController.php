<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use AppHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Kemasan;

class KemasanController extends Controller
{
    public function index(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('m_kemasan');


        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('nama_kemasan','like','%'.$param['search'].'%')
                ->orWhere('keterangan','like','%'.$param['search'].'%');
        }
        
        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->orderBy('id_kemasan', 'desc')
                    ->get()
                );
        if($total_rows){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }
 
    public function show($id)
    {
        $data = Kemasan::find($id, ['id_kemasan','nama_kemasan','keterangan']);
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
    	}else{
	        return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
    	}
        
    }

    public function store(Request $request)
    {
    	
        if($this->_validate($request->all()) === true){
            if(Kemasan::create($request->all())){
                return response()->json(AppHelper::ResponseOK('201'),201);    
            }else{
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
        // $flight = new Flight;

        // $flight->name = $request->name;

        // $flight->save();
    }

    public function update(Request $request, $id)
    {
        if($this->_validate($request->all()) === true){
            $kemasan = Kemasan::findOrFail($id);
            if($kemasan->update($request->all())){
                return response()->json(AppHelper::ResponseOK(),200);
            }else{
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }

    public function delete(Request $request, $id)
    {
        $kemasan = Kemasan::findOrFail($id);
        if($kemasan->delete()){
            return response()->json(AppHelper::ResponseOK(),204);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    }

    public function get_kemasan($id = null){
        $data = Kemasan::all(['id_kemasan AS value','nama_kemasan AS text']);
        
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }

    public function _validate($request){
        $validator = Validator::make($request, [
            'nama_kemasan'  => 'required',
        ],
        [
            'required'  => 'Kolom :attribute harus diisi'
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
            $msg = ''; $x = 1;
            
            foreach ($errors->all() as $message) {
                $msg .= $message;
                if($x < count($errors->all())){
                    $msg .= '<br>,';
                }else{
                    $msg .= '.';
                }
                $x++;  
            }
            return $msg;   
        }else{
            return true;
        }
    }
}
