<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use AppHelper;
use DateTime;
use App\Stok;
use App\Obat;
use App\PurchaseHd;
use App\PurchaseDt;
use App\ReturPurchaseHd;
use App\ReturPurchaseDt;

class PurchaseController extends Controller
{

	public function index(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_purchase_hd AS a')->join('m_supplier AS b', 'b.id_supplier', '=', 'a.id_supplier');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('a.no_faktur','like','%'.$param['search'].'%')
                ->orWhere('b.nama_supplier','like','%'.$param['search'].'%')
                ->orWhere('a.medrep','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.nama_supplier')
                    ->orderBy('a.id', 'desc')
                    ->get()
                );
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function show($id)
    {

        $hd = DB::table('t_purchase_hd AS a')
                ->where('a.id','=',$id)
                ->leftJoin('m_supplier AS b', 'a.id_supplier', '=', 'b.id_supplier')
                ->select('a.id_supplier',
            'b.nama_supplier',
            'a.no_faktur',
            'a.purchasing_date',
            'a.tempo_date',
            'a.total',
            'a.ppn',
            'a.grand_total',
            'a.medrep',
            'a.tempo',
            'a.flag_retur',
            'a.flag_lunas')->first();

        $dt = DB::table('t_purchase_dt AS a')
                ->where('a.id_header','=',$id)
                ->leftJoin('m_obat AS b','a.id_obat','=','b.id_obat')
                ->select('a.id_header',
            'a.id_obat',
            'b.nama_obat',
            'a.batch',
            'a.expired_date',
            'a.qty',
            'a.id_satuan',
            'a.init_satuan',
            'a.harga',
            'a.diskon',
            'a.total',
            'a.ppn',
            'a.grand_total')->get();


        if($hd && $dt){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'header'=> $hd,
                'detail'=> $dt,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'header'=> [],
                'detail'=> [],
            ]),404);
        }
    }

    public function pelunasan(Request $request, $id){
        $hd = PurchaseHd::findOrFail($id);

        if($hd){
            $hd->update(['flag_lunas'=>1]);
            return response()->json(AppHelper::ResponseOK('200'),200); 
        }else{
            return response()->json(AppHelper::FailResponse('404'), 404);
        }
    }

    public function status(Request $request, $id){
        $hd = PurchaseHd::findOrFail($id);
    	$dt = PurchaseDt::where('id_header', $id)->get();

    	if($hd){
    		$returhd = new ReturPurchaseHd;
    		
    		$returhd->id_supplier = $hd->id_supplier;
	    	$returhd->no_faktur = $hd->no_faktur;
	    	$returhd->purchasing_date = $hd->purchasing_date;
	    	$returhd->total = $hd->total;
	    	$returhd->ppn = $hd->ppn;
	    	$returhd->grand_total = $hd->grand_total;
	    	$returhd->medrep = $hd->medrep;
	    	$returhd->tempo = $hd->tempo;
	    	DB::beginTransaction();
	    	if($dt && $returhd->save()){
	    		foreach ($dt as $v) {
	    			$returdt = new ReturPurchaseDt;

	    			$returdt->id_header = $returhd->id;
			    	$returdt->id_obat = $v->id_obat;
			    	$returdt->batch = $v->batch;
			    	$returdt->expired_date = $v->expired_date;
			    	$returdt->qty = $v->qty;
                    $returdt->id_satuan = $v->id_satuan;
			    	$returdt->init_satuan = $v->init_satuan;
			    	$returdt->harga = $v->harga;
			    	$returdt->diskon = $v->diskon;
			    	$returdt->total = $v->total;
			    	$returdt->ppn = $v->ppn;
			    	$returdt->grand_total = $v->grand_total;

			    	if(!$returdt->save()){
			    		$del = ReturPurchaseHd::find($returhd->id);
			    		$del->delete();
			    		DB::rollback();
			    		return response()->json(AppHelper::FailResponse('500'), 500);
			    		break;
			    	}

                    $stock = Stok::where('id_obat', $v->id_obat)->first();
                    if($stock){
                        $mid = ($stock->satuan_mid) ? $stock->satuan_mid : 1;
                        $kecil = ($stock->satuan_kecil) ? $stock->satuan_kecil : 1;

                        if($v->init_satuan === 'B'){
                            $stok = $v->qty * $mid * $kecil;
                        }else if($v->init_satuan === 'S'){
                            $stok = $v->qty * $kecil;
                        }else if($v->init_satuan === 'K'){
                            $stok = $v->qty;
                        }
                        $update = [
                            'id_last_keluar'=>$returhd->id,
                            'stok'  => ($stock->stok - $stok),
                        ];

                        $stock->update($update);
                    }
	    		}
	    		$hd->update(['flag_retur'=>1]);
	    		DB::commit();
	    		return response()->json(AppHelper::ResponseOK('201'),201); 
	    	}else{
	    		return response()->json(AppHelper::FailResponse('500'), 500);
	    	}
    	}else{
    		return response()->json(AppHelper::FailResponse('404'), 404);
    	}
    }

 	public function store(Request $request)
    {
    	$input = $request->all();
    	
        if($this->_validate($input) === true){
        	DB::beginTransaction();
            $flag = false;
            $ins = $upd = 0;
        	try{
                $header = $input['header'];
                if(isset($header['tempo']) && $header['tempo']){
                    $txt = '+'.$header['tempo'].' day';
                    $tempo = new DateTime($header['purchasing_date'].' '.$txt);
                    $header['tempo_date'] = date_format($tempo, 'Y-m-d');
                }
                $header['id_supplier'] = $header['id_supplier']['value'];
                
        		if($id_header = PurchaseHd::create($header)->id){
                    for($i=0;$i<count($input['details']);$i++){
                        $input['details'][$i]['ppn'] = $input['details'][$i]['harga'] * 0.1;
                        $input['details'][$i]['id_header'] = $id_header;
                        $input['details'][$i]['id_obat'] = $input['details'][$i]['id_obat']['value'];

	        			if(!PurchaseDt::create($input['details'][$i])){
	        				DB::rollback();
	        				$flag = false;
	        				return response()->json(AppHelper::FailResponse('500'),500);
	        			}

                        $dt = Stok::where('id_obat', $input['details'][$i]['id_obat'])->first();
                                             
                        if($dt){

                            $mid = ($dt->satuan_mid) ? $dt->satuan_mid : 1;
                            $kecil = ($dt->satuan_kecil) ? $dt->satuan_kecil : 1;
                            
                            if($input['details'][$i]['init_satuan'] === 'B'){
                                $stok = $input['details'][$i]['qty'] * $dt->satuan_mid * $dt->satuan_kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'S'){
                                $stok = $input['details'][$i]['qty'] * $dt->satuan_kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'K'){
                                $stok = $input['details'][$i]['qty'];
                            }
                            
                            $update = [
                                'expired_date' => $input['details'][$i]['expired_date'],
                                'id_last_masuk'=>$input['details'][$i]['id_header'],
                                'stok_awal'=> $dt->stok,
                                'stok'  => ($dt->stok + $stok),
                            ];

                            $dt->update($update);
                            $upd++;
                        }else{
                            $obat = Obat::findOrFail($input['details'][$i]['id_obat']);
                            $mid = ($obat->isi_satuan_mid) ? $obat->isi_satuan_mid : 1;
                            $kecil = ($obat->isi_satuan_kecil) ? $obat->isi_satuan_kecil : 1;

                            if($input['details'][$i]['init_satuan'] === 'B'){
                                $stock = $input['details'][$i]['qty'] * $mid * $kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'S'){
                                $stock = $input['details'][$i]['qty'] * $kecil;
                            }else if($input['details'][$i]['init_satuan'] === 'K'){
                                $stock = $input['details'][$i]['qty'];
                            }

                            $stok = new Stok;

                            $stok->id_obat = $input['details'][$i]['id_obat'];
                            $stok->satuan_besar = 1;
                            $stok->satuan_mid = $obat->isi_satuan_mid;
                            $stok->satuan_kecil = $obat->isi_satuan_kecil;
                            $stok->id_last_masuk = $input['details'][$i]['id_header'];
                            $stok->stok = $stock;
                            $stok->stok_awal = 0;
                            $stok->expired_date = $input['details'][$i]['expired_date'];
                            
                            if(!$stok->save()){
                                DB::rollback();
                                $flag = false;
                                return response()->json(AppHelper::FailResponse('500'),500);
                            }

                            $ins++;
                        }
	        		}
	        		$flag = true;
	        	}else{
	        		$flag = false;
	        	}	
        	} catch(\Exception $e){
        		DB::rollback();
        	}
        	
        	if($flag){
        		DB::commit();
                return response()->json(AppHelper::ResponseOK('true', 'Berhasil, '.$ins.' Barang baru di tambahkan ke Stok dan '.$upd.' Barang di update stok'),201);    
            }else{
            	DB::rollback();
                return response()->json(AppHelper::FailResponse('500'), 500);
            }    
        }else{
            return response()->json(AppHelper::FailResponse('custom', $this->_validate($request->all())), 200);    
        }
    }  

    public function delete($id){
    	$hd = PurchaseHd::findOrFail($id);
        $details = PurchaseDt::where('id_header', $id)->get();
        DB::beginTransaction();
        if($hd->delete()){
            foreach($details as $v){
                $dt = Stok::where('id_obat', $v->id_obat)->first();
                if($dt){
                    $mid = ($dt->satuan_mid) ? $dt->satuan_mid : 1;
                    $kecil = ($dt->satuan_kecil) ? $dt->satuan_kecil : 1;

                    if($v->init_satuan === 'B'){
                        $stok = $v->qty * $dt->satuan_mid * $dt->satuan_kecil;
                    }else if($v->init_satuan === 'S'){
                        $stok = $v->qty * $dt->satuan_kecil;
                    }else if($v->init_satuan === 'K'){
                        $stok = $v->qty;
                    }
                    $update = [
                        //'id_last_keluar'=>$returhd->id,
                        'stok'  => ($dt->stok - $stok),
                    ];

                    $dt->update($update);
                }
            }
        	$delete = PurchaseDt::where('id_header', $id)->delete();
            if($delete){
                DB::commit();
                return response()->json(AppHelper::ResponseOK(),204);
            }else{
                DB::rollback();
                return response()->json(AppHelper::FailResponse('500'), 500);
            }
        }else{
            DB::rollback();
            return response()->json(AppHelper::FailResponse('500'), 500);
        }
    } 

    public function _validate($request){
    	$requestHeader = $request['header'];
    	$requestDetails = $request['details'];
    	
        $validator = Validator::make($requestHeader, [
            //'id_supplier'       => 'required',
            'no_faktur'         => 'required',
            'purchasing_date'   => 'required',
            'tempo'   		   => 'required',
            'total'   		   => 'required',
            'ppn'     		   => 'required',
            'grand_total'       => 'required',
        ],
        [
            'required'  => 'Kolom :attribute harus diisi'
        ]);

        $validator->setAttributeNames([
        	//'id_supplier'	 => 'Nama Supplier',
        	'no_faktur'	     => 'Nomor Faktur',
        	'purchasing_date'=> 'Tgl Pembelian',
        ]);

        $msg = '';
        if($validator->fails()){
            $errors = $validator->errors();
             $x = 1;
            
            foreach ($errors->all() as $message) {
                $msg .= $message;
                if($x < count($errors->all())){
                    $msg .= '<br>,';
                }else{
                    $msg .= '.';
                }
                $x++;  
            }   
            $flag = false;
        }else{
        	$flag = true;
        }
        if($requestDetails){
        	for($i=0; $i<count($requestDetails); $i++){
	        	$validatorDetails = Validator::make($requestDetails[0], [
		            //'id_obat'       => 'required',
		            'id_satuan'     => 'required',
		            'batch'	        => 'required',
		            'expired_date'  => 'required',
		            'qty'   		=> 'required',
		            'harga'   		=> 'required'
		        ],
		        [
		            'required'  => 'Kolom :attribute harus diisi'
		        ]);

		        $validatorDetails->setAttributeNames([
		        	//'id_obat'	=> 'Nama Barang',
		        	'id_satuan'	=> 'Satuan Barang',
		        	'batch'		=> 'No Batch',
		        	'expired_date'=> 'Tgl Kedaluwarsa',
		        	'qty'=> 'Quantity',
		        	'harga'=> 'Harga',
		        ]);

	        	if($validatorDetails->fails()){
	        		$errors = $validatorDetails->errors();
		            $y = 1;
		            
		            foreach ($errors->all() as $message) {
		                $msg .= $message;
		                if($y < count($errors->all())){
		                    $msg .= '<br>,';
		                }else{
		                    $msg .= '.';
		                }
		                $y++;  
		            }
		            $flag = false;
	        	}else{
	        		$flag = true;
	        	}
	        }
        }else{
        	$msg .= '<br> Data barang tidak boleh kosong.';
        }
    	
        
        if($flag){
        	return true;
        }else{
        	return $msg;
        }
        
    }
}
