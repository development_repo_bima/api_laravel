<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use AppHelper;
use App\Stok;

class StokController extends Controller
{
    public function index(Request $request)
    {
        $param = $request->all();
        $offset = ($param['offset'] - 1) * $param['perPage'];
        
        $data = DB::table('t_stok AS a')
                ->leftJoin('m_obat AS b', 'b.id_obat', '=', 'a.id_obat')
                ->leftJoin('t_purchase_hd AS d', 'd.id', '=', 'a.id_last_masuk')
                ->leftJoin('t_sales_hd AS e', 'e.id', '=', 'a.id_last_keluar')
        		->leftJoin('m_kemasan AS c', 'c.id_kemasan', '=', 'b.satuan_kecil');

        if(isset($param['search']) && $param['search']){
            $data = $data
                ->where('b.nama_obat','like','%'.$param['search'].'%');
        }

        $total_rows = $data->count();
        
        $result = collect($data
                    ->skip($offset)
                    ->take($param['perPage'])
                    ->select('a.*', 'b.nama_obat','c.nama_kemasan AS kemasan','d.no_faktur','e.no_order')
                    ->orderBy('b.nama_obat', 'asc')
                    ->get()
                );
        
        if($result){
            return response()->json(array_merge(AppHelper::ResponseOK(), [
                'data'      => $result,
                'total'     => $total_rows,
            ]), 200);    
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'), [
                'data'      => [],
                'total'     => 0,
            ]), 404);
        }
        
    }

    public function show($id)
    {
        $stok = Stok::find($id, [
            'id',
            'id_obat',
            'stok'
        ]);

        if($stok){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'stock'=> $stok
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'stock'=> []
            ]),404);
        }
    }

    public function update(Request $request, $id){
    
        $stok = Stok::findOrFail($id);
        if($stok->update($request->all())){
            return response()->json(AppHelper::ResponseOK(),201);
        }else{
            return response()->json(AppHelper::FailResponse('500'), 500);
        }    
    }

    public function get_obat(){
        
        $data = DB::table('t_stok AS a')
            ->leftJoin('m_obat AS b', 'a.id_obat', '=', 'b.id_obat')
            ->leftJoin('m_kemasan AS c', 'b.satuan_mid', '=', 'c.id_kemasan')
            ->leftJoin('m_kemasan AS d', 'b.satuan_kecil', '=', 'd.id_kemasan')
            ->leftJoin('m_kemasan AS e', 'b.satuan_besar', '=', 'e.id_kemasan')
            ->where('a.stok','>',0)
            ->select('a.id_obat AS value','b.nama_obat AS label', 'b.harga_net', 'b.harga_jual', 'b.satuan_besar','b.satuan_mid','b.satuan_kecil', 'b.isi_satuan_mid', 'b.isi_satuan_kecil', 'e.nama_kemasan AS kemasan_besar','c.nama_kemasan AS kemasan_mid','d.nama_kemasan AS kemasan_kecil')
            ->get();
        
        if($data){
            return response()->json(array_merge(AppHelper::ResponseOK(),[
                'data'=> $data,
            ]),200);
        }else{
            return response()->json(array_merge(AppHelper::FailResponse('404'),[
                'data'=> []
            ]),404);
        }
    }
}
