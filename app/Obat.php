<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
    protected $table = 'm_obat';
	protected $primaryKey = 'id_obat';
    protected $fillable = [
    	'nama_obat',
    	'kode_obat',
    	'margin',
    	'harga_net',
    	'harga_jual',
    	'satuan_besar',
    	'isi_satuan_mid',
    	'satuan_mid',
    	'satuan_kecil',
    	'isi_satuan_kecil'
    ];
}
