<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturSalesHd extends Model
{
    protected $table = 't_retur_sales_hd';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'no_order',
    	'tgl_order',
    	'no_resep',
    	'nama_pasien',
    	'keterangan_resep',

    ];
}
