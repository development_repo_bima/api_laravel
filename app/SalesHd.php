<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesHd extends Model
{
    protected $table = 't_sales_hd';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'no_order',
    	'tgl_order',
    	'no_resep',
    	'nama_pasien',
    	'keterangan_resep',
    	'flag_retur'
    ];
}
