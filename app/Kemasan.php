<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kemasan extends Model
{
	protected $table = 'm_kemasan';
	protected $primaryKey = 'id_kemasan';
    protected $fillable = ['nama_kemasan', 'keterangan'];
}
