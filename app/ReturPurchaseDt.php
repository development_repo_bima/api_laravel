<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturPurchaseDt extends Model
{
    protected $table = 't_retur_purchase_dt';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'id_header',
    	'id_obat',
    	'batch',
    	'expired_date',
    	'qty',
    	'id_satuan',
        'init_satuan',
    	'harga',
    	'diskon',
    	'total',
    	'ppn',
    	'grand_total',
    ];
}
