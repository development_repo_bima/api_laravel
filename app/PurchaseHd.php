<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseHd extends Model
{
    protected $table = 't_purchase_hd';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'id_supplier',
    	'no_faktur',
        'purchasing_date',
    	'tempo_date',
    	'total',
    	'ppn',
    	'grand_total',
    	'medrep',
    	'tempo',
    	'flag_retur',
        'flag_lunas'
    ];
}
