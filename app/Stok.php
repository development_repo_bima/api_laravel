<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $table = 't_stok';
	protected $primaryKey = 'id';
    protected $fillable = [
    	'id_obat',
    	'satuan_besar',
    	'satuan_kecil',
    	'id_last_masuk',
    	'id_last_keluar',
        'stok_awal',
    	'flag_perbaikan',
    	'stok',
        'expired_date'
    ];
}
